from django.urls import path
from . import views

urlpatterns = [
    path('', views.main, name='main'),
    path('foods/', views.foods, name='foods'),
    path('order/', views.order, name='order'),
    path('<slug>', views.foods_detail, name='foods_detail'),
    path('<slug>/like', views.like_food, name='like_food'),
    path('<slug>/dislike', views.dislike_food, name='dislike_food'),
    path('<slug>/comment/<pk>/delete', views.delete_comment, name='delete_comment'),
    path('<slug>/comment/<pk>/edit', views.edit_comment, name='edit_comment'),
    path('image/<int:pk>', views.image, name='image'),
]
