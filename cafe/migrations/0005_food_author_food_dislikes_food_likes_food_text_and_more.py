# Generated by Django 4.0.2 on 2022-03-12 17:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cafe', '0004_remove_food_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='food',
            name='author',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='food',
            name='dislikes',
            field=models.ManyToManyField(related_name='foods_dislikes', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='food',
            name='likes',
            field=models.ManyToManyField(related_name='foods_likes', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='food',
            name='text',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('body', models.TextField()),
                ('date_added', models.DateTimeField(auto_now=True)),
                ('food', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cafe.food')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
