from django import forms

from .models import Comment


class CommentForm(forms.ModelForm):
    body = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'textarea',
        'size': '40',
        'placeholder': 'Напишите свой комментарий и не забудьте поставить лайк ... '
    }))

    class Meta:
        model = Comment
        fields = ['body', ]


