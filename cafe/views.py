from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import *
from .forms import CommentForm
from django.db.models import Q


def main(request):
    objects = Carausel.objects.all()
    search = request.GET.get('input')
    object = objects.filter(Q(title__icontains=search) | Q(text__icontains=search)
                            ) if search else objects
    return render(request, 'main.html', {'objects': objects})


def foods(request):
    food = Food.objects.all()
    category = request.GET.get('category')
    search = request.GET.get('input')
    foods = food.filter(Q(title__icontains=search) | Q(text__icontains=search)
                        ) if search else food
    food = food.filter(category=category) if category else food
    return render(request, 'foods.html', {'food': food})


def order(request):
    order = Order.objects.all()
    search = request.GET.get('input')
    return render(request, 'order.html', {'order': order})


def foods_detail(request, slug):
    food = Food.objects.get(slug=slug)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.food = food
            instance.save()
            return redirect('cafe:foods_detail', slug=slug)
    form = CommentForm()
    search = request.GET.get('input')
    foods = food.filter(Q(title__icontains=search) | Q(text__icontains=search)
                        ) if search else food
    return render(request, 'foods_detail.html', {'food': food, 'form': form})


def like_food(request, slug):
    food = Food.objects.get(slug=slug)
    if request.user not in food.likes.all():
        food.likes.add(request.user)
        food.dislikes.remove(request.user)
    elif request.user in food.likes.all():
        food.likes.remove(request.user)
    return redirect('cafe:foods_detail', slug=slug)


def dislike_food(request, slug):
    food = Food.objects.get(slug=slug)
    if request.user not in food.dislikes.all():
        food.dislikes.add(request.user)
        food.likes.remove(request.user)
    elif request.user in food.dislikes.all():
        food.dislikes.remove(request.user)
    return redirect('cafe:foods_detail', slug=slug)


def delete_comment(request, pk, slug):
    comment = Comment.objects.get(pk=pk)
    if request.method == 'POST':
        comment.delete()
        return redirect('cafe:foods_detail', slug=slug)
    return render(request, 'delete_comment.html', {'comment': comment})


def edit_comment(request, pk, slug):
    comment = Comment.objects.get(pk=pk)
    form = CommentForm(request.POST or None, instance=comment)
    if form.is_valid():
        form.save()
        return redirect('cafe:foods_detail', slug=slug)
    return render(request, 'edit_comment.html', {'form': form, 'food': comment.food})


def image(request, pk):
    object = Carausel.objects.get(pk=pk)
    return render(request, 'image.html', {'object': object})
