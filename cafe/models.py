from django.contrib.auth.models import User
from django.db import models


class Carausel(models.Model):
    image = models.ImageField(upload_to='pics/%y/%m/%d/')


class Food(models.Model):
    title = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=255, )
    category = models.ForeignKey('cafe.Category', on_delete=models.CASCADE)
    image = models.ImageField(default='default_product.png', null=True)
    is_new = models.BooleanField(default=False)
    is_discounted = models.BooleanField(default=False)
    slug = models.SlugField(unique=True)
    date = models.DateField(auto_now=True)
    author = models.ForeignKey(User, default='User', on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, related_name='food_likes')
    dislikes = models.ManyToManyField(User, related_name='food_dislikes')

    def __str__(self):
        return self.title

    def total_likes(self):
        return self.likes.count()

    def total_dislikes(self):
        return self.dislikes.count()

    class Meta:
        db_table = 'cafe_foods'


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'cafe_categories'


class Comment(models.Model):
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    body = models.TextField()
    date_added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.food} - {self.body[:5]}"


class Order(models.Model):
    title = models.CharField(max_length=255)
    sub_title = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    number = models.CharField(max_length=255)

    def __str__(self):
        return self.title
