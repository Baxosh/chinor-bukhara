from django.contrib import admin
from .models import *


class SlugAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Carausel)
admin.site.register(Food, SlugAdmin)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(Order)
